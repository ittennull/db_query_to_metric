use serde::Deserialize;

#[derive(Deserialize, Clone)]
pub struct Configuration {
    pub metric_configurations: Vec<MetricConfiguration>,
}

#[derive(Deserialize, Clone)]
pub struct MetricConfiguration {
    pub metric: Metric,
    pub cron: String,
    pub server: String,
    pub database: String,
    pub collection: String,
    pub query: String,
}

#[derive(Deserialize, Clone)]
pub struct Metric {
    pub name: String,
    pub description: String,
    pub kind: MetricKind,
    pub increment_counter_by_value_of_field: Option<String>,
    pub set_gauge_to_value_of_field: Option<String>,
}

#[derive(Deserialize, Clone, Copy, PartialEq)]
pub enum MetricKind {
    Counter,
    Gauge,
}
