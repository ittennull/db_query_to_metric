use crate::configuration::Configuration;
use std::collections::HashMap;

pub struct CronjobSettings {
    pub app_name: String,
    pub configuration: Configuration,
    pub servers_to_connection_string: HashMap<String, String>,
}
