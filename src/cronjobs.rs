use crate::configuration::{MetricConfiguration, MetricKind};
use crate::cronjob_settings::CronjobSettings;
use crate::metric::Metric;
use crate::query_parser::parse_query;
use anyhow::{anyhow, Context, Result};
use bson::{Bson, Document};
use futures::TryStreamExt;
use log::{error, info};
use mongodb::options::{AggregateOptions, ClientOptions};
use mongodb::Client;
use prometheus_client::registry::Registry;
use std::collections::HashMap;
use tokio_cron_scheduler::{Job, JobScheduler};

pub async fn create_cronjobs(
    settings: CronjobSettings,
    sched: &JobScheduler,
    registry: &mut Registry,
) -> Result<()> {
    let mongo_clients = create_mongodb_clients(&settings).await?;

    for metric_cfg in settings.configuration.metric_configurations {
        let metric = Metric::new(
            metric_cfg.metric.kind,
            metric_cfg.metric.name.clone(),
            metric_cfg.metric.description.clone(),
            registry,
        );

        let client = mongo_clients[&metric_cfg.server].clone();
        let app_name = settings.app_name.clone();

        // schedule a query cronjob
        sched
            .add(Job::new_async(
                metric_cfg.cron.clone().as_str(),
                move |_, _| {
                    let metric = metric.clone();
                    let client = client.clone();
                    let metric_cfg = metric_cfg.clone();
                    let app_name = app_name.clone();

                    Box::pin(async {
                        let metric_name = metric_cfg.metric.name.clone();
                        if let Err(e) = run_query(app_name, client, metric_cfg, metric).await {
                            error!("Failed to run query for {}, error: {:#}", metric_name, e);
                        }
                    })
                },
            )?)
            .await?;
    }

    Ok(())
}

async fn run_query(
    app_name: String,
    client: Client,
    metric_cfg: MetricConfiguration,
    metric: Metric,
) -> Result<()> {
    let db = client.database(&metric_cfg.database);
    let collection = db.collection::<Document>(&metric_cfg.collection);

    let query = parse_query(&metric_cfg.query)?;

    let mut options = AggregateOptions::default();
    options.comment = Some(format!(
        "{}: running a query for metric {}",
        app_name, metric_cfg.metric.name
    ));
    let mut cursor = collection.aggregate(query, Some(options)).await?;

    // clear gauge up, it will be fully repopulated by the results of a database query.
    // it saves me from remembering labels and setting the corresponding values to zero when the query returns no documents for a particular label
    if metric_cfg.metric.kind == MetricKind::Gauge {
        metric.clear();
    }

    let mut doc_count = 0;
    while let Some(doc) = cursor.try_next().await? {
        info!(
            "Query '{}' result {}: {:?}",
            metric_cfg.metric.name,
            doc_count + 1,
            doc
        );

        update_metric_value(&metric, &metric_cfg, doc)?;

        doc_count += 1;
    }

    info!(
        "Query for metric '{}' returned {} documents",
        metric_cfg.metric.name, doc_count
    );

    Ok(())
}

fn update_metric_value(
    metric: &Metric,
    metric_cfg: &MetricConfiguration,
    mut doc: Document,
) -> Result<()> {
    let field_with_update_value = match metric_cfg.metric.kind {
        MetricKind::Counter => &metric_cfg.metric.increment_counter_by_value_of_field,
        MetricKind::Gauge => &metric_cfg.metric.set_gauge_to_value_of_field,
    };

    let update_value = match field_with_update_value {
        None => 1,
        Some(field) => doc
            .remove(field)
            .with_context(|| anyhow!("Document doesn't have field `{}`", field))?
            .as_i32()
            .with_context(|| anyhow!("Can't parse field `{}` as an integer", field))?
            as i64,
    };

    let labels = doc
        .into_iter()
        .map(|(key, value)| {
            let value = match value {
                Bson::String(s) => s,
                x => x.to_string(),
            };
            (key, value)
        })
        .collect();

    metric.update(&labels, update_value)?;

    Ok(())
}

async fn create_mongodb_clients(settings: &CronjobSettings) -> Result<HashMap<String, Client>> {
    let mut mongo_clients: HashMap<String, Client> = HashMap::default();
    for (name, connection_string) in &settings.servers_to_connection_string {
        let mut client_options = ClientOptions::parse(&connection_string).await?;
        client_options.app_name = Some(settings.app_name.clone());

        let client = Client::with_options(client_options)?;
        mongo_clients.insert(name.clone(), client);
    }

    Ok(mongo_clients)
}
