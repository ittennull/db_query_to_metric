FROM rust AS build
WORKDIR /src
COPY . .
RUN cargo build --release

FROM debian:bookworm-slim AS bin
WORKDIR /app
COPY --from=build /src/target/release/db_query_to_metric /src/log4rs.yaml ./
ENTRYPOINT ["./db_query_to_metric"]
