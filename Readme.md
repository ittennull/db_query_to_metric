**db_query_to_metric** converts results of MongoDb queries to prometheus metrics that are exposed on `/metrics` endpoint.

## Motivation
Sometimes it's necessary to have some heavy queries that for example check integrity of the data in a database. While it's possible to run a query right after a data modification, it brings a considerable load on the database. Moreover, not always it is needed to run the query so often, it could be once per hour or daily

Querying alone isn't much useful, the query results can tell you more once they are plotted on a chart. On top of that, an alerting can prove to be a life savior.

While Grafana handles visualization well and Alertmanager takes care of alerts, I lacked means to query a database and make metrics out of the results

This project fills this gap without trying to reinvent the rest.

## How to run locally
Create a file [config.yaml](config.yaml) with metric definitions, then run this

```shell
docker run --rm -it \
  -e "CONNECTION_STRING_MYSERVER=mongodb://localhost/?directConnection=true" \
  -v ./config.yaml:/app/config.yaml \
  -p 8080:8080 \
  registry.gitlab.com/ittennull/db_query_to_metric:main
```
If `config.yaml` has several values for `server` key, all the connection strings must be added to the `docker run -e ...` arguments

## How to use in kubernetes
1. Deploy a container **db_query_to_metric**
2. Provide configuration in `config.yaml` file and put it to a _configmap_ that is used by the pod as a file
2. Set up a [PodMonitor](https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/api.md#monitoring.coreos.com/v1.PodMonitor) to pull the metrics
3. Set up dashboards and alerting (outside of the scope of this document)

You can find an example in folder [k8s-deployment-example](./k8s-deployment-example)

Replace the configuration in `configmap.yml`, change the secret value in `secret.yml` and run the command
```shell
kubectl apply -n <your-namespace> -f ./k8s-deployment-example
```
